#!/usr/bin/env python3.8

import time

from BotballKit import Bot, Sensor, SensorType, Servo

# Create the bot object
bot = Bot()

# Create the sensor objects
left_sensor = Sensor(bot, SensorType.DIGITAL, 2)
right_sensor = Sensor(bot, SensorType.DIGITAL, 1)

# Create the servo objects
vertical_servo = Servo(bot, 0)
horizontal_servo = Servo(bot, 1)

# Define the variable that determines which servo to move
# True will move vertical_servo and False will move horizontal_servo
vertical = True


if __name__ == '__main__':
    # Enable the servos
    vertical_servo.enable(1024)
    horizontal_servo.enable(0)

    while True:
        # Check for pressing both of the sensors
        if left_sensor.status() and right_sensor.status():
            # Switch the servo
            vertical = not vertical
            print('Switched servo.')

            # Wait for one sensor to be unpressed to prevent repeated switching
            while left_sensor.status() and right_sensor.status():
                continue
            continue

        # Get the servo object
        servo = vertical_servo if vertical else horizontal_servo

        # Move the servo down/left if the left sensor is being pressed
        if left_sensor.status():
            servo.position(servo.position() + 10)

        # Move the servo up/right if the right sensor is being pressed
        if right_sensor.status():
            servo.position(servo.position() - 10)

        # Wait to prevent /really/ fast servo movements
        time.sleep(0.01)
